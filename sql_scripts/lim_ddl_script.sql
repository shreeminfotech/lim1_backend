CREATE TABLE lim_reporting.`lim_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `full_name` varchar(45) NOT NULL,
  `password` varchar(64) NOT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `desg_id` int(11) NOT NULL,  
  `temp_token` int(32) NULL ,
  `f_temp_password` tinyint(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);
 
CREATE TABLE lim_reporting.`lim_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(265)  NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,  
  PRIMARY KEY (`role_id`)
);
 
CREATE TABLE lim_reporting.`lim_user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,  
  KEY `user_fk_idx` (`user_id`),
  KEY `role_fk_idx` (`role_id`),
  CONSTRAINT `role_fk` FOREIGN KEY (`role_id`) REFERENCES `lim_roles` (`role_id`),
  CONSTRAINT `user_fk` FOREIGN KEY (`user_id`) REFERENCES `lim_users` (`user_id`)
);

CREATE TABLE lim_reporting.`lim_designation` (
  `desg_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `code` varchar(10) NOT NULL,
  `description` varchar(265)  NULL,
  `department`  varchar(45) NOT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,  
   UNIQUE KEY `name_UNIQUE` (`name`),
  PRIMARY KEY (`desg_id`)
);


CREATE TABLE lim_reporting.`lim_experiments` (
  `experiment_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(265)  NULL,
  `reason`  varchar(265) NOT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,  
   UNIQUE KEY `name_UNIQUE` (`name`),
  PRIMARY KEY (`experiment_id`)
);

CREATE TABLE lim_reporting.`lim_prj_template_doc` (
  `document_id` bigint NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `document_format` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `template_type` varchar(255) DEFAULT NULL,
  `upload_dir` varchar(255) DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ;

CREATE TABLE lim_reporting.`lim_prj_type` (
  `type_id` bigint NOT NULL,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(265)  NULL,  
  `enabled` tinyint(4) DEFAULT NULL,  
  `created_by` varchar(255) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ;


CREATE TABLE lim_reporting.`lim_sequences` (
  `seq_id` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` bigint(20) NOT NULL,
  UNIQUE KEY `name_UNIQUE` (`name`),
  PRIMARY KEY (`seq_id`)
) ENGINE=InnoDB;


/* Meta Insertions*/
INSERT INTO `lim_reporting`.`lim_designation` (`desg_id`, `name`, `code`, `description`, `department`, `enabled`) VALUES ('1', 'System Admin', 'SYSADMIN' , 'IT System Adminstrator' ,  'Information Technology' ,  1);
INSERT INTO `lim_reporting`.`lim_roles` (`role_id`, `description`, `name`) VALUES ('1', 'super admin', 'ROLE_ADMIN');
INSERT INTO `lim_reporting`.`lim_users` (`user_id`, `enabled`, `username`, `email`, `full_name`, `password` , `desg_id` ) VALUES ('1', 1, 'admin@lim.com',  'admin@lim.com', 'Admin', '$2y$12$oi0uZE4Xf/PJdqvSmGTQRu39yfaTH22z3/pVuUi1EQ8PpazthZY/m' , 1);
INSERT INTO `lim_reporting`.`lim_user_roles` (`user_id`, `role_id`) VALUES ('1', '1');

/*Project Type Insertions*/
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (2 , 'Partial Validation Method development', 'PVMD', 'Partial Validation Method development', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
 
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (3 , 'Method validation', 'VM', 'Method validation', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());

INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (4 , 'Addendum', 'ADM', 'Addendum', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());

INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (5 , 'Partial method validation', 'PVM', 'Partial method validation', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
 
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (6 , 'Subjects samples Analysis', 'SSA', 'Subjects samples Analysis', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
