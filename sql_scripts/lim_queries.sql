SELECT * FROM lim_reporting.lim_users;
SELECT * FROM lim_reporting.lim_roles;
SELECT * FROM lim_reporting.lim_user_roles;
SELECT * FROM lim_reporting.lim_designation;
SELECT * FROM lim_reporting.lim_prj_template_doc;

SELECT * FROM lim_reporting.lim_user_roles usrRoles INNER JOIN lim_reporting.lim_users on users.user_oid = usrRoles.user_id;


INSERT INTO `lim_reporting`.`lim_designation` (`desg_id`, `name`, `code`, `description`, `department`, `enabled`) VALUES ('1', 'System Admin', 'SYSADMIN' , 'IT System Adminstrator' ,  'Information Technology' ,  1);
INSERT INTO `lim_reporting`.`lim_roles` (`role_id`, `description`, `name`) VALUES ('1', 'super admin', 'ROLE_ADMIN');
INSERT INTO `lim_reporting`.`lim_users` (`user_id`, `enabled`, `username`, `email`, `full_name`, `password` , `desg_id` ) VALUES ('1', 1, 'admin@lim.com',  'admin@lim.com', 'Admin', '$2y$12$oi0uZE4Xf/PJdqvSmGTQRu39yfaTH22z3/pVuUi1EQ8PpazthZY/m' , 1);
INSERT INTO `lim_reporting`.`lim_user_roles` (`user_id`, `role_id`) VALUES ('1', '1');

INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (1 , 'Method development', 'MD', 'Method development', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
 
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (2 , 'Partial Validation Method development', 'PVMD', 'Partial Validation Method development', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
 
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (3 , 'Method validation', 'VM', 'Method validation', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());

INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (4 , 'Addendum', 'ADM', 'Addendum', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());

INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (5 , 'Partial method validation', 'PVM', 'Partial method validation', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
 
INSERT INTO `lim_reporting`.`lim_prj_type` (`type_id`, `name`, `code`, `description`, `enabled`, 
`created_by`, `creation_date`, `last_modified_by`, `last_modified_date`)
 VALUES (6 , 'Subjects samples Analysis', 'SSA', 'Subjects samples Analysis', '1', 'admin@lim.com', curdate(), 'admin@lim.com', curdate());
