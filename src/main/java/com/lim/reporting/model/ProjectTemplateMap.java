package com.lim.reporting.model;

import org.modelmapper.PropertyMap;

import com.lim.reporting.persistent.entity.ProjectTemplateDocument;

public class ProjectTemplateMap extends PropertyMap<ProjectTemplateDTO, ProjectTemplateDocument> {

	@Override
	protected void configure() {
		skip().setId(null);
		skip().setDocumentFormat(null);
		skip().setFileName(null);
		skip().setUploadDir(null);
		skip().setUserId(null);
		skip().setCreationDate(null);
		skip().setCreatedBy(null);
		skip().setLastModifiedBy(null);
		skip().setLastModifiedDate(null);
	}

}
