package com.lim.reporting.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "UI User object with all details.")
@JsonInclude(Include.NON_EMPTY)
public class UserRequest {

	private String userName;
	private String fullName;
	private String email;
	private String password;
	private boolean active;

	@JsonIgnoreProperties(value = "users")
	private Set<RoleDTO> roles;

	private DesignationDTO designation;
	private String confiramtionToken;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<RoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleDTO> roles) {
		this.roles = roles;
	}

	public DesignationDTO getDesignation() {
		return designation;
	}

	public void setDesignation(DesignationDTO designation) {
		this.designation = designation;
	}

	public String getConfiramtionToken() {
		return confiramtionToken;
	}

	public void setConfiramtionToken(String confiramtionToken) {
		this.confiramtionToken = confiramtionToken;
	}

}
