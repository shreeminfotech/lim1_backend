package com.lim.reporting.model;

import org.modelmapper.PropertyMap;

import com.lim.reporting.persistent.entity.User;

public class UserMap extends PropertyMap<UserRequest, User> {

	@Override
	protected void configure() {
		skip().setPassword(null);
		skip().setId(null);
		skip().setConfirmationToken(null);
		skip().setTempPassword(null);
		skip().setCreationDate(null);
		skip().setCreatedBy(null);
		skip().setLastModifiedBy(null);
		skip().setLastModifiedDate(null);
		skip().getDesignation().setDepartment(null);
		skip().getDesignation().setDescription(null);
		skip().getDesignation().setActive(null);
		skip().getDesignation().setCreationDate(null);
		skip().getDesignation().setCreatedBy(null);
		skip().getDesignation().setLastModifiedBy(null);
		skip().getDesignation().setLastModifiedDate(null);
	}

}
