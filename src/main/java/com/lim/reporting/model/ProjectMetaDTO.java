package com.lim.reporting.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ProjectMetaDTO {

	private List<RoleDTO> roles;
	private List<ExperimentDTO> experiments;
	private List<ProjectTemplateDTO> templates;
	private List<ProjectTypeDTO> types;
	private Integer projectTypeId;

	public List<RoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleDTO> roles) {
		this.roles = roles;
	}

	public Integer getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(Integer projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public List<ExperimentDTO> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<ExperimentDTO> experiments) {
		this.experiments = experiments;
	}

	public List<ProjectTemplateDTO> getTemplates() {
		return templates;
	}

	public void setTemplates(List<ProjectTemplateDTO> templates) {
		this.templates = templates;
	}

	public List<ProjectTypeDTO> getTypes() {
		return types;
	}

	public void setTypes(List<ProjectTypeDTO> types) {
		this.types = types;
	}

}
