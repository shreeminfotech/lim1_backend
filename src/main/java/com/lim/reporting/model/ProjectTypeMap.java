package com.lim.reporting.model;

import org.modelmapper.PropertyMap;

import com.lim.reporting.persistent.entity.ProjectType;

public class ProjectTypeMap extends PropertyMap<ProjectTypeDTO, ProjectType> {

	@Override
	protected void configure() {
		skip().setId(null);
		skip().setDescription(null);
		skip().setActive(null);
		skip().setCreationDate(null);
		skip().setCreatedBy(null);
		skip().setLastModifiedBy(null);
		skip().setLastModifiedDate(null);
		
	}

}
