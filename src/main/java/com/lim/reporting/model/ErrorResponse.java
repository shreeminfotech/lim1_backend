package com.lim.reporting.model;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

	/**
	 * HTTP error code,
	 */
	private final int code;

	/**
	 * Textual representation of the HTTP error
	 */
	private final String phrase;

	/**
	 * Detail message to clients
	 */
	private final String message;

	protected ErrorResponse(HttpStatus status, String detailMessage) {
		this.code = status.value();
		this.phrase = status.getReasonPhrase();
		this.message = detailMessage;
	}

	public static ErrorResponse of(HttpStatus status) {
		return of(status, null);
	}

	public static ErrorResponse of(HttpStatus status, Exception ex) {
		return new ErrorResponse(status, ex.getMessage());
	}

	public int getCode() {
		return code;
	}

	public String getPhrase() {
		return phrase;
	}

	public String getMessage() {
		return message;
	}

}
