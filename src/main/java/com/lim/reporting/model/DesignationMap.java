package com.lim.reporting.model;

import org.modelmapper.PropertyMap;

import com.lim.reporting.persistent.entity.Designation;

public class DesignationMap extends PropertyMap<DesignationDTO, Designation> {

	@Override
	protected void configure() {
		skip().setId(null);
		skip().setCreationDate(null);
		skip().setCreatedBy(null);
		skip().setLastModifiedBy(null);
		skip().setLastModifiedDate(null);
	}

}
