package com.lim.reporting.model;

import org.modelmapper.PropertyMap;

import com.lim.reporting.persistent.entity.Role;

public class RoleMap extends PropertyMap<RoleDTO, Role> {

	@Override
	protected void configure() {
		//skip().setId(null);
		skip().setCreationDate(null);
		skip().setCreatedBy(null);
		skip().setLastModifiedBy(null);
		skip().setLastModifiedDate(null);
		skip().setUsers(null);
	}

}
