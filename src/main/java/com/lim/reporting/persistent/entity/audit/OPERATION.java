package com.lim.reporting.persistent.entity.audit;

public enum OPERATION {
	
	/**
	 * OPERATION perform on entity classes
	 */
	INSERT, UPDATE, DELETE;

	private String value;

	OPERATION() {
		value = toString();
	}

	public String getValue() {
		return value;
	}

	public static OPERATION parse(final String value) {
		OPERATION operation = null;
		for (final OPERATION op : OPERATION.values()) {
			if (op.getValue().equals(value)) {
				operation = op;
				break;
			}
		}
		return operation;
	}
}
