package com.lim.reporting.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lim_sequences")
public class LIMSequence {

	public LIMSequence() {
	}

	public LIMSequence(Long id, String name, Long value) {
		this.id = id;
		this.name = name;
		this.value = value;
	}

	@Id
	@Column(name = "seq_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "value")
	private Long value;

	@Column(name = "name")
	private String name;

}
