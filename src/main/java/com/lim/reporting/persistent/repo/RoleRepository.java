package com.lim.reporting.persistent.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lim.reporting.persistent.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	/*
	 * Find Role by User Identifier
	 */
	Optional<Role> findByName(String name);

	/*
	 * Lists all roles in table.
	 */
	public List<Role> findAll();

	/*
	 * Create Role
	 */
	public Role save(Role role);

	/*
	 * Delete User
	 */
	public void delete(Role role);
}
