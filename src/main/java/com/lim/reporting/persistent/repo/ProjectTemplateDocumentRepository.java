package com.lim.reporting.persistent.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lim.reporting.persistent.entity.ProjectTemplateDocument;

public interface ProjectTemplateDocumentRepository extends JpaRepository<ProjectTemplateDocument, Integer> {

	/*
	 * Find Document by Identifier
	 */
	Optional<ProjectTemplateDocument> findById(Long Id);

	@Query("Select a from ProjectTemplateDocument a where user_id = ?1 and template_type = ?2")
	ProjectTemplateDocument checkDocumentByUserId(Long userId, String templateType);

	/*
	 * Lists all ProjectTemplateDocument in table.
	 */
	public List<ProjectTemplateDocument> findAll();

	/*
	 * Find Document stored path.
	 */
	@Query("Select fileName from ProjectTemplateDocument a where user_id = ?1 and template_type = ?2")
	String getUploadDocumentPath(String code);

	/*
	 * Create ProjectTemplateDocument
	 */
	public ProjectTemplateDocument save(ProjectTemplateDocument document);

	/*
	 * Delete ProjectTemplateDocument
	 */
	public void delete(ProjectTemplateDocument document);
}
