package com.lim.reporting.persistent.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lim.reporting.persistent.entity.ProjectType;

public interface ProjectTypeRepository extends JpaRepository<ProjectType, Integer> {

	/*
	 * Lists all ProjectType in table.
	 */
	public List<ProjectType> findAll();

}
