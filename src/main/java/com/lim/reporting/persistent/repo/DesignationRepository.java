package com.lim.reporting.persistent.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lim.reporting.persistent.entity.Designation;

public interface DesignationRepository extends JpaRepository<Designation, Integer> {

	/*
	 * Find Designation by Identifier
	 */
	Optional<Designation> findById(Long Id);

	/*
	 * Find Designation by code
	 */
	Optional<Designation> findByCode(String code);

	/*
	 * Lists all Designations in table.
	 */
	public List<Designation> findAll();

	/*
	 * Create Designation
	 */
	public Designation save(Designation designation);

	/*
	 * Delete Designation
	 */
	public void delete(Designation designation);
}
