package com.lim.reporting.persistent.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lim.reporting.persistent.entity.Experiment;

public interface ExperimentRepository extends JpaRepository<Experiment, Integer> {

	/*
	 * Find Experiment by Identifier
	 */
	Optional<Experiment> findById(Long Id);

	/*
	 * Find Experiment by code
	 */
	Optional<Experiment> findByCode(String code);

	/*
	 * Lists all Experiments in table.
	 */
	public List<Experiment> findAll();

	/*
	 * Create Experiment
	 */
	public Experiment save(Experiment designation);

	/*
	 * Delete Experiment
	 */
	public void delete(Experiment designation);
}
