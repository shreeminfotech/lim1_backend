package com.lim.reporting.persistent.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lim.reporting.persistent.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	Optional<User> findByEmail(String email);

	/*
	 * Lists all users in table.
	 */
	public List<User> findAll();

	/*
	 * Find User by User Identifier
	 */
	public Optional<User> findById(String userId);

	/*
	 * Find User by Token
	 */
	User findByConfirmationToken(String confirmationToken);

	/*
	 * Create User
	 */
	public User save(User user);

	/*
	 * Delete User
	 */
	public void delete(User user);
}
