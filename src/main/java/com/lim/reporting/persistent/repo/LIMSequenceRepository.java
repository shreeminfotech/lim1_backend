package com.lim.reporting.persistent.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lim.reporting.persistent.entity.LIMSequence;

public interface LIMSequenceRepository extends JpaRepository<LIMSequence, Integer> {

	@Modifying
	@Query(nativeQuery = true, value = "UPDATE lim_sequences SET value = LAST_INSERT_ID(value + 1) WHERE name = name")
	public Integer updateNextValueByName(@Param("name") String name);

	@Query(nativeQuery = true, value = "SELECT LAST_INSERT_ID()")
	public Integer getNextValue();
}
