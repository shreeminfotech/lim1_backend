package com.lim.reporting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.DesignationDTO;
import com.lim.reporting.service.DesignationService;

@RequestMapping("/admin/designations")
@RestController
public class DesignationController {

	@Autowired
	private DesignationService designationService;

	@PostMapping
	public DesignationDTO saveDesignation(@RequestBody DesignationDTO designationDto) {
		return designationService.createDesignation(designationDto);
	}

	@GetMapping
	public List<DesignationDTO> getAllDesignations() {
		return designationService.getDesignations();
	}
}
