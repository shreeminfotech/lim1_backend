package com.lim.reporting.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.AuthenticationRequest;
import com.lim.reporting.model.AuthenticationResponse;
import com.lim.reporting.service.LIMUserDetailsService;
import com.lim.reporting.service.UserService;
import com.lim.reporting.util.JwtUtil;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class HomeController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private LIMUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtTokenUtil;
	
	@Autowired
	private UserService userService;
	
	@GetMapping(value = "/home")
	public String home() {
		return("<h1>Welcom Admin</h1>");
	}

	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
						authenticationRequest.getUsername(), 
						authenticationRequest.getPassword()));
		} catch (AuthenticationException e) {
			throw new Exception("Invalid username or password", e);
		}

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		return ResponseEntity.ok(
				new AuthenticationResponse(
						jwtTokenUtil.generateToken(userDetails)));
	}
	
	@GetMapping(value = "/users/confirm")
	public String confirm(@RequestParam("token") String token) {
		userService.confirmrUser(token);
		return "User confirmed.";
	}
}
