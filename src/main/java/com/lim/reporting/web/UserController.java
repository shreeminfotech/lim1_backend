package com.lim.reporting.web;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.UserRequest;
import com.lim.reporting.model.UserResponse;
import com.lim.reporting.service.EmailService;
import com.lim.reporting.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RequestMapping("/admin/users")
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmailService emailService;

	@ApiOperation(notes = "Magic happens", value = "This method is used to create users of clients.")
	@PostMapping
	public UserResponse saveUser(@ApiParam(value = "UserRequest") @RequestBody UserRequest userRequest) throws MessagingException {
		UserResponse userResponse = userService.createUser(userRequest);
		if (userResponse != null) {
			emailService.sendMail(userResponse);
		}
		return userResponse;
	}

	@ApiOperation(notes = "Magic happens", value = "This method is used to find all active users of clients.")
	@GetMapping
	public List<UserResponse> getAllUsers() {
		return userService.getAllUsers();
	}

}
