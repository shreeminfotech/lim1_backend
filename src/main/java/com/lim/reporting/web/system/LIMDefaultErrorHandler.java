package com.lim.reporting.web.system;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lim.reporting.model.ErrorResponse;

/**
 * Able to handle unmapped requests
 * 
 * @author reddy
 *
 */

@Controller
public class LIMDefaultErrorHandler {

	@RequestMapping
	public ResponseEntity<ErrorResponse> handleUnmappedRequest(final HttpServletRequest request) {
		return ResponseEntity.status(NOT_FOUND).body(ErrorResponse.of(NOT_FOUND));
	}
}
