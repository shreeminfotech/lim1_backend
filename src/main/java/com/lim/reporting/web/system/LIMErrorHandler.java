package com.lim.reporting.web.system;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.lim.reporting.model.ErrorResponse;
import com.lim.reporting.web.exception.InvalidRequestException;

@RestControllerAdvice
public class LIMErrorHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AuthenticationException.class)
	public ResponseEntity<Object> handlerAccessDeniedException(final AuthenticationException authException) {
		return handleExceptionInternal(authException, FORBIDDEN);
	}
	
	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<Object> handlerInvalidRequestException(final InvalidRequestException badRequestException) {
		return handleExceptionInternal(badRequestException, BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		ErrorResponse errorResponse = ErrorResponse.of(status, ex);
		return super.handleExceptionInternal(ex, errorResponse, headers, status, request);
	}

	private ResponseEntity<Object> handleExceptionInternal(Exception ex, HttpStatus status) {
		return handleExceptionInternal(ex, null, null, status, null);
	}
}
