package com.lim.reporting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.ExperimentDTO;
import com.lim.reporting.service.ExperimentService;

@RequestMapping("/admin/experiments")
@RestController
public class ExperimentController {

	@Autowired
	private ExperimentService experimentService;

	@PostMapping
	public ExperimentDTO saveExperiment(@RequestBody ExperimentDTO experimentDto) {
		return experimentService.createExperiment(experimentDto);
	}

	@GetMapping
	public List<ExperimentDTO> getAllExperiments() {
		return experimentService.getExperiments();
	}
}
