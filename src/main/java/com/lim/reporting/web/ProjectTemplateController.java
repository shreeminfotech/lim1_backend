package com.lim.reporting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lim.reporting.model.ProjectTemplateDTO;
import com.lim.reporting.service.ProjectTemplateDocumentService;

@RequestMapping("/admin/templates")
@RestController
public class ProjectTemplateController {

	@Autowired
	private ProjectTemplateDocumentService templateService;

	@PostMapping
	public ProjectTemplateDTO uploadFile(@RequestParam MultipartFile file, @RequestParam String jsonPayload)
			throws JsonMappingException, JsonProcessingException {
		ProjectTemplateDTO requestObject = mapPayloadToObject(jsonPayload, file);
		return templateService.uploadFile(requestObject);
	}

//	private UploadFileDTO buildUploadFileObject(ProjectTemplateDTO templateResponse) {
//		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
//				.path(templateResponse.getFileName()).toUriString();
//
//		UploadFileDTO uploadFile = new UploadFileDTO(templateResponse.getFileName(), fileDownloadUri,
//				projectTemplate.getFile().getContentType(), projectTemplate.getFile().getSize());
//		return uploadFile;
//	}

	private ProjectTemplateDTO mapPayloadToObject(String jsonPayload, MultipartFile file)
			throws JsonProcessingException, JsonMappingException {
		ProjectTemplateDTO projectTemplate = new ObjectMapper().readValue(jsonPayload, ProjectTemplateDTO.class);
		projectTemplate.setFile(file);
		return projectTemplate;
	}

	@GetMapping
	public List<ProjectTemplateDTO> getAllTemplates() {
		return templateService.getTemplates();
	}
}
