package com.lim.reporting.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.ProjectMetaDTO;
import com.lim.reporting.service.ProjectMetaService;

@RequestMapping("/admin/meta")
@RestController
public class ProjectMetaController {

	@Autowired
	private ProjectMetaService metaService;
	
	@GetMapping
	public ProjectMetaDTO getMeta() {
		return metaService.getMeta();
	}

}
