package com.lim.reporting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.model.RoleDTO;
import com.lim.reporting.service.RoleService;

@RequestMapping("/admin/roles")
@RestController
public class RoleController {

	@Autowired
	private RoleService roleService;

	@PostMapping
	public RoleDTO saveUser(@RequestBody RoleDTO roleDto) {
		return roleService.createRole(roleDto);
	}

	@GetMapping
	public List<RoleDTO> getAllRoles() {
		return roleService.getAllRoles();
	}
}
