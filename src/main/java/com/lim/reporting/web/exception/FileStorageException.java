package com.lim.reporting.web.exception;

public class FileStorageException extends LIMApplicationException {

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}

	private static final long serialVersionUID = 1L;
}
