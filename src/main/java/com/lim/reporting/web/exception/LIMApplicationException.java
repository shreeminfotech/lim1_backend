package com.lim.reporting.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class LIMApplicationException extends RuntimeException {

	public LIMApplicationException(String message) {
		super(message);
	}

	public LIMApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	private static final long serialVersionUID = 1L;

}
