package com.lim.reporting.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lim.reporting.config.properties.mail.MailConfigurationSettings;
import com.lim.reporting.service.LIMSequenceService;

@RestController
public class Status {
	
	@Autowired
	private MailConfigurationSettings mailSettings;
	
	@Autowired
	private LIMSequenceService limSequenceService;
	

    @GetMapping("/status")
    public String getStatus() {
    	System.out.println( mailSettings.getName() +"/"+mailSettings.getServer().getUsername() + "/" + mailSettings.getSmtp().getQuitwait());
        return "ok" + limSequenceService.getNextValue("PROJECT_TYPE_SEQ_ID");
    }

}
