package com.lim.reporting.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.persistent.repo.LIMSequenceRepository;

@Service
@Transactional
public class LIMSequenceService {

	@Autowired
	LIMSequenceRepository limSequenceRepository;

	public Integer getNextValue(String name) {
		limSequenceRepository.updateNextValueByName(name);
		return limSequenceRepository.getNextValue();
	}
}
