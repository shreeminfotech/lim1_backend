package com.lim.reporting.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.RoleDTO;
import com.lim.reporting.persistent.entity.Role;
import com.lim.reporting.persistent.repo.RoleRepository;
import com.lim.reporting.web.exception.InvalidRequestException;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private ModelMapper modelMapper;

	public RoleDTO createRole(RoleDTO roleRequest) {
		// Validate RoleDTO before we persist data.
		validate(roleRequest);

		// Convert User Request to Entity to persist.
		Role role = mapRoleDtoToRole(roleRequest);

		// Save
		Role roleObj = roleRepository.save(role);

		return mapRoleToRoleDTO(roleObj);

	}

	private RoleDTO mapRoleToRoleDTO(Role roleObj) {
		RoleDTO uiRole = modelMapper.map(roleObj, RoleDTO.class);
		return uiRole;
	}

	public List<RoleDTO> getAllRoles() {
		List<Role> roles = roleRepository.findAll();
		List<RoleDTO> uiRoles = roles.stream().map(role -> {
			RoleDTO uiRole = modelMapper.map(role, RoleDTO.class);
			uiRole.getUsers().stream().forEach(userDTO -> {
				userDTO.setPassword(null);
				userDTO.setConfiramtionToken(null);
				userDTO.setDesignation(null);
				userDTO.setUserName(null);
				userDTO.setEmail(null);
				//userDTO.setActive(null);
			});
			return uiRole;
		}).collect(Collectors.toList());

		return uiRoles;
	}

	private Role mapRoleDtoToRole(RoleDTO roleRequest) {
		return modelMapper.map(roleRequest, Role.class);
	}

	private void validate(RoleDTO roleRequest) throws InvalidRequestException {
		if (StringUtils.isEmpty(roleRequest.getRoleName())) {
			throw new InvalidRequestException("Invalid Request.");
		}

		Optional<Role> roleExists = roleRepository.findByName(roleRequest.getRoleName());
		if (!roleExists.isEmpty()) {
			throw new InvalidRequestException(roleRequest.getRoleName() + " already exists!.");
		}
	}

}
