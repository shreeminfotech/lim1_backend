package com.lim.reporting.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.ProjectTypeDTO;
import com.lim.reporting.persistent.entity.ProjectType;
import com.lim.reporting.persistent.repo.ProjectTypeRepository;

@Service
public class ProjectTypeService {

	@Autowired
	private ProjectTypeRepository projectRepository;

	@Autowired
	private ModelMapper modelMapper;

	public List<ProjectTypeDTO> getProjectTypes() {
		List<ProjectType> projectTypes = projectRepository.findAll();
		List<ProjectTypeDTO> uiProjectTypes = projectTypes.stream().map(projectType -> {
			ProjectTypeDTO uiProjectType = modelMapper.map(projectType, ProjectTypeDTO.class);
			return uiProjectType;
		}).collect(Collectors.toList());

		return uiProjectTypes;
	}
}
