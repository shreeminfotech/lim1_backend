package com.lim.reporting.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.lim.reporting.persistent.entity.LIMUserDetails;
import com.lim.reporting.persistent.entity.User;
import com.lim.reporting.persistent.repo.UserRepository;

@Service
public class LIMUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + email));
		return new LIMUserDetails(user.get());
	}
}
