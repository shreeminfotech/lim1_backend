package com.lim.reporting.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.DesignationDTO;
import com.lim.reporting.persistent.entity.Designation;
import com.lim.reporting.persistent.repo.DesignationRepository;
import com.lim.reporting.web.exception.InvalidRequestException;

@Service
public class DesignationService {

	@Autowired
	private DesignationRepository designationRepository;

	@Autowired
	private ModelMapper modelMapper;

	public DesignationDTO createDesignation(DesignationDTO designationDTO) {
		// Validate RoleDTO before we persist data.
		validate(designationDTO);

		// Convert User Request to Entity to persist.
		Designation designation = mapDesignationDTOToDesignation(designationDTO);

		// Save
		Designation designationObj = designationRepository.save(designation);

		return mapDesignationToDesignationDTO(designationObj);

	}

	private DesignationDTO mapDesignationToDesignationDTO(Designation designationObj) {
		DesignationDTO uiDesignation = modelMapper.map(designationObj, DesignationDTO.class);
		return uiDesignation;
	}

	public List<DesignationDTO> getDesignations() {
		List<Designation> designations = designationRepository.findAll();
		List<DesignationDTO> uiRoles = designations.stream().map(designation -> {
			DesignationDTO uiDesignation = modelMapper.map(designation, DesignationDTO.class);
			return uiDesignation;
		}).collect(Collectors.toList());

		return uiRoles;
	}

	private Designation mapDesignationDTOToDesignation(DesignationDTO designationDTO) {
		return modelMapper.map(designationDTO, Designation.class);
	}

	private void validate(DesignationDTO designationDTO) throws InvalidRequestException {
		if (StringUtils.isEmpty(designationDTO.getName())) {
			throw new InvalidRequestException("Invalid Request.");
		}

		Optional<Designation> designationExists = designationRepository.findByCode(designationDTO.getCode());
		if (!designationExists.isEmpty()) {
			throw new InvalidRequestException(designationDTO.getName() + " already exists!.");
		}
	}

}
