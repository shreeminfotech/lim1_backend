package com.lim.reporting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.ExperimentDTO;
import com.lim.reporting.model.ProjectMetaDTO;
import com.lim.reporting.model.ProjectTemplateDTO;
import com.lim.reporting.model.ProjectTypeDTO;
import com.lim.reporting.model.RoleDTO;

@Service
public class ProjectMetaService {

	private static final String PROJECT_TYPE_SEQ_ID = "PROJECT_TYPE_SEQ_ID";
	
	@Autowired
	private ExperimentService experimentService;
	
	@Autowired
	private ProjectTypeService projectTypeService;
	
	@Autowired
	private ProjectTemplateDocumentService documentService;

	@Autowired	
	private RoleService roleService;
	
	@Autowired
	private LIMSequenceService limSequenceService;

	public ProjectMetaDTO getMeta() {
		ProjectMetaDTO metaDTO = new ProjectMetaDTO();
		metaDTO.setRoles(getRoles());
		metaDTO.setExperiments(getExperiments());
		metaDTO.setTemplates(getTemplates());
		metaDTO.setTypes(getProjectType());
		metaDTO.setProjectTypeId(getProjectTypeID());
		return metaDTO;

	}
	
	private Integer getProjectTypeID() {
		return limSequenceService.getNextValue(PROJECT_TYPE_SEQ_ID);
	}
	
	private List<ExperimentDTO> getExperiments() {
		return experimentService.getExperiments();
	}
	
	private List<ProjectTypeDTO> getProjectType() {
		return projectTypeService.getProjectTypes();
	}
	
	private List<ProjectTemplateDTO> getTemplates(){
		return documentService.getTemplates();
	}
	
	private List<RoleDTO> getRoles(){
		return roleService.getAllRoles();
	}
}
