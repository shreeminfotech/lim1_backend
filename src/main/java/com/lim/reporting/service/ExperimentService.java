package com.lim.reporting.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.ExperimentDTO;
import com.lim.reporting.persistent.entity.Experiment;
import com.lim.reporting.persistent.repo.ExperimentRepository;
import com.lim.reporting.web.exception.InvalidRequestException;

@Service
public class ExperimentService {

	@Autowired
	private ExperimentRepository experimentRepository;

	@Autowired
	private ModelMapper modelMapper;

	public ExperimentDTO createExperiment(ExperimentDTO experimentDTO) {
		// Validate ExperimentDTO before we persist data.
		validate(experimentDTO);

		// Convert User Request to Entity to persist.
		Experiment experiment = mapExperimentDTOToExperiment(experimentDTO);

		// Save
		Experiment experimentObj = experimentRepository.save(experiment);

		return mapExperimentToExperimentDTO(experimentObj);

	}

	private ExperimentDTO mapExperimentToExperimentDTO(Experiment experimentObj) {
		ExperimentDTO uiExperiment = modelMapper.map(experimentObj, ExperimentDTO.class);
		return uiExperiment;
	}

	public List<ExperimentDTO> getExperiments() {
		List<Experiment> experiments = experimentRepository.findAll();
		List<ExperimentDTO> uiExperiments = experiments.stream().map(experiment -> {
			ExperimentDTO uiExperiment = modelMapper.map(experiment, ExperimentDTO.class);
			return uiExperiment;
		}).collect(Collectors.toList());

		return uiExperiments;
	}

	private Experiment mapExperimentDTOToExperiment(ExperimentDTO experimentDTO) {
		return modelMapper.map(experimentDTO, Experiment.class);
	}

	private void validate(ExperimentDTO experimentDTO) throws InvalidRequestException {
		if (StringUtils.isEmpty(experimentDTO.getName())) {
			throw new InvalidRequestException("Invalid Request.");
		}

		Optional<Experiment> experimentExists = experimentRepository.findByCode(experimentDTO.getCode());
		if (!experimentExists.isEmpty()) {
			throw new InvalidRequestException(experimentDTO.getName() + " already exists!.");
		}
	}

}
