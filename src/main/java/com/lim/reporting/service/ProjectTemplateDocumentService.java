package com.lim.reporting.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.lim.reporting.config.properties.file.FileConfigurationSettings;
import com.lim.reporting.model.ProjectTemplateDTO;
import com.lim.reporting.persistent.entity.LIMUserDetails;
import com.lim.reporting.persistent.entity.ProjectTemplateDocument;
import com.lim.reporting.persistent.repo.ProjectTemplateDocumentRepository;
import com.lim.reporting.util.UserSessionUtil;
import com.lim.reporting.web.exception.FileStorageException;

@Service
public class ProjectTemplateDocumentService {

	private Path filePath = null;

	@Autowired
	private UserSessionUtil userSession;

	@Autowired
	private ProjectTemplateDocumentRepository templateDocumentRepository;

	@Autowired
	private FileConfigurationSettings fileConfigurationSettings;

	@Autowired
	private ModelMapper modelMapper;

	@PostConstruct
	void init() {
		this.filePath = Paths.get(fileConfigurationSettings.getUploadDirectory()).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.filePath);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	public ProjectTemplateDTO uploadFile(ProjectTemplateDTO projectTemplate) {
		// Save File to directory.
		saveToDirectory(projectTemplate.getFile());

		// Get user from session.
		Long userIdToSavingDoc = getUser().getUser().getId();

		// Convert User Request to Entity to persist.
		ProjectTemplateDocument templateDoc = mapProjectTemplateDTO(projectTemplate);

		// save to database
		templateDoc = mapToProjectTemplateDocument(templateDoc, userIdToSavingDoc, projectTemplate.getFile());
		templateDoc = templateDocumentRepository.save(templateDoc);
		return mapExperimentToExperimentDTO(templateDoc);
	}

	public List<ProjectTemplateDTO> getTemplates() {
		List<ProjectTemplateDocument> templates = templateDocumentRepository.findAll();
		List<ProjectTemplateDTO> uiTemplates = templates.stream().map(template -> {
			ProjectTemplateDTO uiTemplate = modelMapper.map(template, ProjectTemplateDTO.class);
			return uiTemplate;
		}).collect(Collectors.toList());
		return uiTemplates;
	}

	private ProjectTemplateDTO mapExperimentToExperimentDTO(ProjectTemplateDocument templateObj) {
		ProjectTemplateDTO uiResponse = modelMapper.map(templateObj, ProjectTemplateDTO.class);
		return uiResponse;
	}

	private ProjectTemplateDocument mapProjectTemplateDTO(ProjectTemplateDTO projectTemplate) {
		return modelMapper.map(projectTemplate, ProjectTemplateDocument.class);
	}

	private void saveToDirectory(MultipartFile file) {
		// Normalize file name
		String fileName = getFileName(file);
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.filePath.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	private LIMUserDetails getUser() {
		return userSession.userInSession().get();
	}

	private ProjectTemplateDocument mapToProjectTemplateDocument(ProjectTemplateDocument newDoc, Long userId,
			MultipartFile file) {
		newDoc.setUserId(userId);
		newDoc.setDocumentFormat(file.getContentType());
		newDoc.setFileName(getFileName(file));
		newDoc.setUploadDir(fileConfigurationSettings.getUploadDirectory());
		return newDoc;
	}

	private String getFileName(MultipartFile file) {
		return StringUtils.cleanPath(file.getOriginalFilename());
	}

}
