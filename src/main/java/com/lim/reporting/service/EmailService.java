package com.lim.reporting.service;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.lim.reporting.model.UserResponse;

@Service
public class EmailService {

	@Autowired
	private TemplateEngine emailTemplateEngine;

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${webServerUrl}")
	private String webServerUrl;

	private static final String EMAIL_SIMPLE_TEMPLATE_NAME = "mailTemplate.html";

	public void sendMail(UserResponse user) throws MessagingException {
		Context context = new Context();
		context.setVariable("user", user);
		context.setVariable("url", webServerUrl);
		String process = emailTemplateEngine.process(EMAIL_SIMPLE_TEMPLATE_NAME, context);
		javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		helper.setSubject("Welcome " + user.getFullName());
		helper.setText(process, true);
		helper.setTo(user.getEmail());
		javaMailSender.send(mimeMessage);
	}
}