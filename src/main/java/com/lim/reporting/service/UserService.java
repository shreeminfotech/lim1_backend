package com.lim.reporting.service;

import static com.lim.reporting.util.PasswordUtil.generateTempPassword;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lim.reporting.model.UserRequest;
import com.lim.reporting.model.UserResponse;
import com.lim.reporting.persistent.entity.User;
import com.lim.reporting.persistent.repo.UserRepository;
import com.lim.reporting.web.exception.InvalidRequestException;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ModelMapper modelMapper;

	public UserResponse createUser(UserRequest userRequest) {

		// Validate UserRequest before we persist data.
		validate(userRequest);

		String tempPassword = generateTempPassword();

		// Convert User Request to Entity to persist.
		User user = mapUserRequestToUser(userRequest, tempPassword);

		// Save
		User userObject = userRepository.save(user);

		return mapUserToUserResponse(userObject, tempPassword);
	}

	public User confirmrUser(String token) {
		User user = userRepository.findByConfirmationToken(token);

		if (user == null) {
			throw new InvalidRequestException("Invalid token.");
		}
		// Token found
		user.setActive(Boolean.TRUE);
		user.setConfirmationToken("");

		// Save user
		userRepository.save(user);
		return user;
	}

	public List<UserResponse> getAllUsers() {
		List<User> users = userRepository.findAll();
		List<UserResponse> userResponses = users.stream().map(user -> {
			UserResponse usersResponse = modelMapper.map(user, UserResponse.class);
			usersResponse.setPassword(StringUtils.EMPTY);
			return usersResponse;
		}).collect(Collectors.toList());

		return userResponses;
	}

	private UserResponse mapUserToUserResponse(User user, String tempPassword) {
		UserResponse userResponse = modelMapper.map(user, UserResponse.class);
		userResponse.setPassword(tempPassword);
		userResponse.setConfiramtionToken(user.getConfirmationToken());
		return userResponse;
	}

	private User mapUserRequestToUser(UserRequest userRequest, String tempPassword) {
		User user = modelMapper.map(userRequest, User.class);
		String encodedPassword = passwordEncoder.encode(tempPassword);

		// Temporary password
		user.setPassword(encodedPassword);

		// user will be activated once click email link
		user.setActive(false);
		user.setTempPassword(true);

		// Generate random 36-character string token for confirmation link
		user.setConfirmationToken(UUID.randomUUID().toString());

		return user;
	}

	private void validate(UserRequest userRequest) throws InvalidRequestException {
		if (StringUtils.isEmpty(userRequest.getPassword()) || StringUtils.isEmpty(userRequest.getEmail())) {
			throw new InvalidRequestException("Invalid Request.");
		}

		Optional<User> userExists = userRepository.findByEmail(userRequest.getEmail());
		if (!userExists.isEmpty()) {
			throw new InvalidRequestException(userRequest.getEmail() + " already registered.");
		}
	}

}
