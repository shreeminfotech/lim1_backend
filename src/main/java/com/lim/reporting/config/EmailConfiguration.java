package com.lim.reporting.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.lim.reporting.config.properties.mail.MailConfigurationSettings;

@Configuration
public class EmailConfiguration {

	@Autowired
	private MailConfigurationSettings mailConfigurationSettings;

	public static final String EMAIL_TEMPLATE_ENCODING = "UTF-8";

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailConfigurationSettings.getServer().getHost());
		mailSender.setUsername(mailConfigurationSettings.getServer().getUsername());
		mailSender.setPassword(mailConfigurationSettings.getServer().getPassword());
		mailSender.setPort(mailConfigurationSettings.getServer().getPort());
		mailSender.setProtocol(mailConfigurationSettings.getServer().getProtocol());

		Properties mailProperties = mailSender.getJavaMailProperties();
		mailProperties.put("mail.smtp.auth", mailConfigurationSettings.getSmtp().getAuth());
		mailProperties.put("mail.smtp.starttls.enable", mailConfigurationSettings.getSmtp().getStarttls());

		return mailSender;
	}

	@Bean
	public TemplateEngine emailTemplateEngine() {
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		// Resolver for HTML emails (except the editable one)
		templateEngine.addTemplateResolver(htmlTemplateResolver());
		return templateEngine;
	}

	private ITemplateResolver htmlTemplateResolver() {
		final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(1));
		//	templateResolver.setResolvablePatterns(Collections.singleton("templates/*"));
		templateResolver.setPrefix("/mail/templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding(EMAIL_TEMPLATE_ENCODING);
		templateResolver.setCacheable(false);
		return templateResolver;
	}

}
