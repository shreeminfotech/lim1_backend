package com.lim.reporting.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lim.reporting.model.DesignationMap;
import com.lim.reporting.model.ExperimentMap;
import com.lim.reporting.model.ProjectTemplateMap;
import com.lim.reporting.model.ProjectTypeMap;
import com.lim.reporting.model.RoleMap;
import com.lim.reporting.model.UserMap;

@Configuration
public class CommonConfiguration {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		customizeModelMapper(modelMapper);
		modelMapper.validate();
		return modelMapper;
	}

	private void customizeModelMapper(ModelMapper modelMapper) {
		modelMapper.addMappings(new UserMap());
		modelMapper.addMappings(new RoleMap());
		modelMapper.addMappings(new DesignationMap());
		modelMapper.addMappings(new ExperimentMap());
		modelMapper.addMappings(new ProjectTemplateMap());
		modelMapper.addMappings(new ProjectTypeMap());
	}
}
