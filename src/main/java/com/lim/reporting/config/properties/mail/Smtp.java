package com.lim.reporting.config.properties.mail;

public class Smtp {

	private Boolean auth;
	private Boolean starttls;
	private Boolean quitwait;

	public Boolean getAuth() {
		return auth;
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public Boolean getStarttls() {
		return starttls;
	}

	public void setStarttls(Boolean starttls) {
		this.starttls = starttls;
	}

	public Boolean getQuitwait() {
		return quitwait;
	}

	public void setQuitwait(Boolean quitwait) {
		this.quitwait = quitwait;
	}
}