package com.lim.reporting.config.properties.mail;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = { "classpath:mail/email-config.properties", "classpath:mail/java-mail.properties" })
@ConfigurationProperties("mail")
public class MailConfigurationSettings {

	/**
	 * java-mail.properties
	 */
	private Smtp smtp;

	/**
	 * email-config.properties
	 */
	private Server server;

	private String name;

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public Smtp getSmtp() {
		return smtp;
	}

	public void setSmtp(Smtp smtp) {
		this.smtp = smtp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
