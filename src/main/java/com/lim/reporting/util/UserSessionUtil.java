package com.lim.reporting.util;

import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.lim.reporting.persistent.entity.LIMUserDetails;

@Component
public class UserSessionUtil {

	public Optional<LIMUserDetails> userInSession() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null || !authentication.isAuthenticated()) {
			return null;
		}
		return Optional.of(((LIMUserDetails) authentication.getPrincipal()));
	}
}
