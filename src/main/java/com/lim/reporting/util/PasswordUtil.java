package com.lim.reporting.util;

import java.security.SecureRandom;
import java.util.Random;

public class PasswordUtil {

	private static final Random RANDOM = new SecureRandom();
	private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final Integer PASSWORD_TOKEN_LENGTH = 9;

	public static String generateTempPassword() {
		StringBuilder returnValue = new StringBuilder(PASSWORD_TOKEN_LENGTH);
		for (int i = 0; i < PASSWORD_TOKEN_LENGTH; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		return new String(returnValue);
	}

}
